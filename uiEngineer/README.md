# Joyjet tech interview

This is a test to analyze knowledge in UI Engineer development, using specifics resources.

- This test is designed to evaluate your knowledge of UI engineer.
- You will have 5 days to complete the test.
- The test includes a simple project that you will work on and save on a repository.
- The project is to create a responsive landing page for a fictional product called "SPACE".
- The landing page should include a hero section, features section, testimonials section, pricing section, and a call-to-action section.
- The landing page should be responsive and work on desktop and mobile devices.
- You are expected to write clean, maintainable, and well-organized code.
- After completing the project, you should submit a link to the repository where you saved the project.

## Guidelines

- [Duplicate](https://help.github.com/articles/duplicating-a-repository/) this repository (do **not** fork it);
- Resolve your test;
- Layout is in [Figma](https://www.figma.com/file/c8VICidlA7IGE2t9BelIyY/joyjet-recruitment-web?node-id=0%3A1&t=jFnafFSnDEluvlfn-1) and PDF [download](layout/joyjet-recruitment-web.pdf).

## Features

The project must be built following a writing standard established by you, and the project should be **responsive**.

- ReactJS / NextJS;
- HTML5;
- CSS3;
- SCSS;
- Grid: [Flexbox Grid](https://github.com/hugeinc/flexboxgrid-sass) or [Bootstrap 4+ SCSS Version](https://getbootstrap.com/docs/4.0/getting-started/theming/);
- Icons: [Elegant Icons](https://github.com/josephnle/elegant-icons);
- Font: [Poppins](https://fonts.google.com/specimen/Poppins);
- Figma.
 
 You should document the components created for the site in some way, such as [Storybook](https://storybook.js.org/), feel free to choose the tool that best fits your test.

## Requirements

1. Hero section:
- A full-width background image or video with a heading and subheading centered on top of it.
- A call-to-action button with a "click" label.
2. Features section:
- A section that showcases the features of the product with icons and short descriptions.
- The section should be divided into 3 equal columns on desktop and a single column on mobile.
3. Testimonials section:
- A section that displays testimonials from satisfied customers.
- The section should include a headshot of the customer, their name, and a quote.
4. Pricing section:
- A section that displays the pricing plans for the product.
- The section should include 3 pricing plans, each with a title, price, features list, and a call-to-action button.
5. Call-to-action section:
- A section that encourages the user to newsletter for the product.
- The section should include a heading, subheading, and a call-to-action button.


Example :

![Alt Text](images/story.gif)

## Differentials

- Indented code is a pleasure to look at;
- Using [Tailwindcss](https://tailwindcss.com/docs/installation) or [Bootstrap 5.3 SCSS Version](https://getbootstrap.com/docs/5.3/getting-started/introduction/)
- Never use images wider than 1920px (unless this is really necessary or specified);
- You can use Pug to create static pages, but if you use React.js it will be a big differential;
- Effects and animations will receive bonus points;
- SVG Animations of shapes;
- WebGL Animations with Three.js or Babylon.js;
- Animations with Framer X;
- Sticky navigation bar (blue stripe on the middle) should be fixed on top after scrolling on page;
- Git is essential, but organization is also essential, remember this;
- Remember : [image compression](https://tinypng.com/) is important\!

This is a model of a sticky navigation-bar, fixed on top after scrolling on page.